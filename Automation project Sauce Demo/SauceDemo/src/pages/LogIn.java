package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogIn {
    WebDriver driver;

    By user = By.id("user-name");
    By password = By.id("password");
    By logIn = By.id("login-button");
    By invalid = By.cssSelector("h3[data-test='error']");
    By valid = By.cssSelector("span[class*='title']");

    public LogIn(WebDriver driver){

        this.driver = driver;

    }

    public WebElement setEmail(){

       return driver.findElement(user);

    }

    public WebElement setPassword(){

       return driver.findElement(password);

    }
    
    public WebElement logIn1() {
    	return driver.findElement(logIn);
    }
    
    public WebElement valid() {
    	return driver.findElement(valid);
    }
    
    public WebElement invalid() {
    	return driver.findElement(invalid);
    }

    public void logIn(){
        this.setEmail();
        this.setPassword(); 
        this.logIn1();
        this.valid();       
        this.invalid();   
}
    
    public void logIn(String users, String passwords) {
		driver.findElement(user).sendKeys(users);
		driver.findElement(password).sendKeys(passwords);
		driver.findElement(logIn).click();
		
	}

}

