package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Form {
	WebDriver driver;
	
	By firstname = By.id("first-name");
	By lastname = By.id("last-name");
	By postalcode = By.id("postal-code");
	By continue1 = By.id("continue");
	By title = By.cssSelector("span[class='title']");
	
	public Form(WebDriver driver) {
		this.driver = driver;
	}
	
	public void form(String firstname1, String lastname1, int postalcode1) {
		String str = String.valueOf(postalcode1);
		driver.findElement(firstname).sendKeys(firstname1);
		driver.findElement(lastname).sendKeys(lastname1);
		driver.findElement(postalcode).sendKeys(str);
		driver.findElement(continue1).click();
	}
	
	public String getTitle() {
		return driver.findElement(title).getText();
	}
	
}
