package pages;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Order {
	WebDriver driver;
	
	By backpack = By.id("add-to-cart-sauce-labs-backpack");
	By light = By.id("add-to-cart-sauce-labs-bike-light");
	By tshirt = By.id("add-to-cart-sauce-labs-bolt-t-shirt");
	By jacket = By.id("add-to-cart-sauce-labs-fleece-jacket");
	By onesie = By.id("add-to-cart-sauce-labs-onesie");
	By tshirtred = By.id("add-to-cart-test.allthethings()-t-shirt-(red)");
	
	By numproducts = By.cssSelector("span[class='shopping_cart_badge']");
	By cart = By.className("shopping_cart_link");
	By checkout = By.id("checkout");
	By messagge = By.cssSelector("span[class='title']");
	By sorting = By.cssSelector("select[class='product_sort_container']");
	By options = By.tagName("option");
	
	By rbackpack = By.id("remove-sauce-labs-backpack");
	By rlight = By.id("remove-sauce-labs-bike-light");
	By rtshirt = By.id("remove-sauce-labs-bolt-t-shirt");
	By rjacket = By.id("remove-sauce-labs-fleece-jacket");
	By ronesie = By.id("remove-sauce-labs-onesie");
	By rtshirtred = By.id("remove-test.allthethings()-t-shirt-(red)");
	
	List<WebElement> list = new ArrayList<WebElement>();
	List<By> rlist = new ArrayList<By>();
	
	
	public Order(WebDriver driver){

        this.driver = driver;

    }
	
	
	public String getMessagge() {
		return driver.findElement(messagge).getText();
	}
	
	public int getNumProducts() {
		String str = driver.findElement(numproducts).getText();
		int i=Integer.parseInt(str);
		return i;
	}
	public WebElement jacket() {
		return driver.findElement(jacket);
	}
	public WebElement backpack() {
		return driver.findElement(backpack);
	}
	public WebElement light() {
		return driver.findElement(light);
	}
	public WebElement onesie() {
		return driver.findElement(onesie);
	}
	public WebElement tshirtred() {
		return driver.findElement(tshirtred);
	}
	public WebElement tshirt() {
		return driver.findElement(tshirt);
	}
	
	public WebElement rjacket() {
		return driver.findElement(rjacket);
	}
	public WebElement rbackpack() {
		return driver.findElement(rbackpack);
	}
	public WebElement rlight() {
		return driver.findElement(rlight);
	}
	public WebElement ronesie() {
		return driver.findElement(ronesie);
	}
	public WebElement rtshirtred() {
		return driver.findElement(rtshirtred);
	}
	public WebElement rtshirt() {
		return driver.findElement(rtshirt);
	}
	 
	public void sorting(int i) throws IOException {
		List<WebElement> listoptions;
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2));
		wait.until(ExpectedConditions.visibilityOfElementLocated(sorting));
		listoptions = driver.findElements(options); 
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(".//screenshot/imagebeforesorting"+i+".png"));
		
		driver.findElement(sorting).click();
		listoptions.get(i).click();
		
		File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile2, new File(".//screenshot/imageaftersorting"+i+".png"));
	}
	
	
	   public void order(int items) throws InterruptedException {
	   list.add(0, driver.findElement(backpack));
	   list.add(1, driver.findElement(jacket));
	   list.add(2, driver.findElement(light));
	   list.add(3, driver.findElement(onesie));
	   list.add(4, driver.findElement(tshirt));
	   list.add(5, driver.findElement(tshirtred));
	   //int size = list.size();
		  
		   for (int i=0;i<(items);i++) {
			   //int rd = rand.nextInt(size);
			   WebElement wb = list.get(i);
			   wb.click();
			   Thread.sleep(1000);
		   }
		}
	   
	   
	   public void removeItems(int rem) throws InterruptedException {
		   rlist.add(0, rbackpack);
		   rlist.add(1, rjacket);
		   rlist.add(2, rlight);
		   rlist.add(3, ronesie);
		   rlist.add(4, tshirt);
		   rlist.add(5, tshirtred);
		   WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
		   for (int i=0;i<rem;i++) {
			   By wbr = rlist.get(i);
			   wait.until(ExpectedConditions.visibilityOfElementLocated(wbr));
			   driver.findElement(wbr).click();
			   Thread.sleep(1000);
		   }
		} 
	   
	   public int getFinalItems(int items, int rem) {
		   return items-rem;
	   }
	   
	   public void placeOrder(){
		   driver.findElement(cart).click();
		   
		   driver.findElement(checkout).click();
	   }
	 
	 
}
