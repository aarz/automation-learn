package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Shipping {
	WebDriver driver;
	
	By finish = By.id("finish");
	By message = By.cssSelector("div[id='checkout_complete_container'] h2");

	public Shipping(WebDriver driver) {
		this.driver = driver;
	}


	public void shipping() {
		driver.findElement(finish).click();
	}
	
	public String getMessage() {
		//Thread.sleep(1000);
		return driver.findElement(message).getText();
	}
}
