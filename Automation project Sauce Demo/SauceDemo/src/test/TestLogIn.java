package test;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.LogIn;


public class TestLogIn{
	WebDriver driver;
	LogIn signin1;
	Logger logger;
	@BeforeEach
	public void setup() {
		 System.setProperty("webdriver.chrome.driver",	  "C:\\Users\\ana__\\Downloads\\chromedriver.exe"); 
		  driver = new	  ChromeDriver(); 
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.get("https://www.saucedemo.com"	  );
		  signin1 = new LogIn(driver); 
		  logger = LogManager.getLogger(TestLogIn.class.getName());

	}
	
	@AfterEach
	public void finish() {
		driver.quit();
	}
	
	@ParameterizedTest(name = "{index} => users={0}, passwords={1}")
	@CsvFileSource(files = "src/test/resources/TestInvalidData.csv", delimiter = ',')
	  public void testLogInInvalidData(String users, String passwords) throws InterruptedException {
 
	  signin1.logIn(users, passwords);
	  assertEquals("Epic sadface: Username and password do not match any user in this service",signin1.invalid().getText());
	  logger.error("Login with invalid credentials"); 

	  }
	 
	@ParameterizedTest(name = "{index} => users={0}, passwords={1}")
	@CsvFileSource(files = "src/test/resources/TestValidData.csv", delimiter = ',')
	  public void testLogInValidData(String users, String passwords) throws InterruptedException {

	  signin1.logIn(users, passwords);
	  assertEquals("PRODUCTS",signin1.valid().getText());
	  logger.info("Login with valid credentials"); 

	  }


}
