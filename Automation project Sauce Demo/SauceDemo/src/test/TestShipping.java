package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.Form;
import pages.LogIn;
import pages.Order;
import pages.Shipping;

public class TestShipping {
	WebDriver driver;
	LogIn login1;
	Order o1;
	Form f1;
	Shipping s1;
	Logger logger;
	@BeforeEach
	public void setup() {
		 System.setProperty("webdriver.chrome.driver",	  "C:\\Users\\ana__\\Downloads\\chromedriver.exe"); 
		  driver = new	  ChromeDriver(); 
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.get("https://www.saucedemo.com"	  );
		  login1 = new LogIn(driver); 
		  o1 = new Order(driver);
		  f1 = new Form(driver);
		  s1 = new Shipping(driver);
		  logger = LogManager.getLogger(TestShipping.class.getName());

	}
	
	  @AfterEach 
	  public void finish() { 
		  driver.quit(); 
	  }
	 
	 
	
	@ParameterizedTest(name = "{index} => users={0}, passwords={1}, items={2}, firstnames={3}, lastnames={4}, postalcodes={5}, rem={6}")
	//@ParameterizedTest(name = "{index} => users={0}, passwords={1}")
	@CsvFileSource(files = "src/test/resources/TestShipping.csv", delimiter = ',')
	  public void testShipping(String users, String passwords, int items, String firstnames, String lastnames, int postalcodes, int rem) throws InterruptedException, IOException {

	  login1.logIn(users, passwords);
	  logger.info("Login with valid credentials");
	  o1.sorting(3);
	  logger.info("Sorting items");
	  o1.order(items);
	  logger.info("Selecting items"); 
	  o1.removeItems(rem); 
	  assertEquals(o1.getFinalItems(items, rem), o1.getNumProducts());
	  logger.info("Removing items"); 
	  o1.placeOrder();
	  logger.info("Place order adding items"); 
	  f1.form(firstnames,lastnames,postalcodes);
	  logger.info("Filling form"); 
	  s1.shipping();
	  assertEquals("THANK YOU FOR YOUR ORDER",s1.getMessage());
	  logger.info("Final placing order"); 
	  
	  }


}
