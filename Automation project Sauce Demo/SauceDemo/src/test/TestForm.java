package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import pages.Form;
import pages.LogIn;
import pages.Order;

public class TestForm {
	WebDriver driver;
	LogIn login1;
	Order o1;
	Form f1;
	Logger logger;
	@BeforeEach
	public void setup() {
		 System.setProperty("webdriver.chrome.driver",	  "C:\\Users\\ana__\\Downloads\\chromedriver.exe"); 
		  driver = new	  ChromeDriver(); 
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.get("https://www.saucedemo.com"	  );
		  login1 = new LogIn(driver); 
		  o1 = new Order(driver);
		  f1 = new Form(driver);
		  logger = LogManager.getLogger(TestForm.class.getName());
	}
	
	  @AfterEach 
	  public void finish() { 
		  driver.quit(); 
	  }
	
	@ParameterizedTest(name = "{index} => users={0}, passwords={1}, items={2}, firstnames={3}, lastnames={4}, postalcodes={5}")
	@CsvFileSource(files = "src/test/resources/TestForm.csv", delimiter = ',')
	  public void testForm(String users, String passwords, int items, String firstnames, String lastnames, int postalcodes) throws InterruptedException {
	     
	  login1.logIn(users, passwords);
	  logger.info("Login with valid credentials");
	  o1.order(items);
	  logger.info("Adding items");
	  o1.placeOrder();
	  logger.info("Place order");
	  f1.form(firstnames,lastnames,postalcodes);
	  assertEquals("CHECKOUT: OVERVIEW",f1.getTitle());
	  logger.info("Filling form"); 
	  
	  }

}
