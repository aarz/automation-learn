package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.LogIn;
import pages.Order;

/**
 * @author Ana
 *
 */
public class TestOrder {
	WebDriver driver;
	LogIn login1;
	Order o1;
	Logger logger;
	@BeforeEach
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.saucedemo.com");
		login1 = new LogIn(driver);
		o1 = new Order(driver);
		logger = LogManager.getLogger(TestOrder.class.getName());

	}
	
	  @AfterEach 
	  public void finish() { 
		  driver.quit(); 
	  }
	
	  @ParameterizedTest(name = "{index} => users={0}, passwords={1}, items={2}")
	  
	  @CsvFileSource(files = "src/test/resources/TestOrder.csv", delimiter = ',')
	  public void testOrder(String users, String passwords, int items) throws
	  InterruptedException, IOException {
	  
	  login1.logIn(users, passwords); 
	  o1.sorting(3);
	  o1.sorting(2);
	  logger.info("Sorting items"); 
	  o1.order(items);
	  assertEquals(items,o1.getNumProducts());
	  logger.info("Adding items"); 
	  o1.placeOrder();
	  assertEquals("CHECKOUT: YOUR INFORMATION",o1.getMessagge());
	  logger.info("Place order adding items successful"); 

	  }
	 

	@ParameterizedTest(name = "{index} => users={0}, passwords={1}, items={2}, rem={3}")
	@CsvFileSource(files = "src/test/resources/TestOrderRemove.csv", delimiter = ',')
	public void testOrderRemove(String users, String passwords, int items, int rem) throws InterruptedException {

		login1.logIn(users, passwords);
		logger.info("Login with valid credentials"); 
		o1.order(items);
		logger.info("Selecting items"); 
		o1.removeItems(rem); 
		assertEquals(o1.getFinalItems(items, rem), o1.getNumProducts());
		logger.info("Removing items succsessful"); 
		o1.placeOrder();
		assertEquals("CHECKOUT: YOUR INFORMATION", o1.getMessagge());
		logger.info("Place order adding and removing items successful"); 

		

	}
}
