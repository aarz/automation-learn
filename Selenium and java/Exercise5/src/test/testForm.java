package test;

import static org.junit.Assert.assertEquals;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.readExcel;
import excel.FillForm;
public class testForm{
	WebDriver driver;
@Test(dataProvider="testdata")
public void demoClass(String firstname, String lastname, String job) throws InterruptedException {
System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver.exe");
WebDriver driver = new ChromeDriver();
driver.get("https://formy-project.herokuapp.com/form");
excel.FillForm ff = new excel.FillForm(driver);
ff.setFirstName().sendKeys(firstname);
ff.setLastName().sendKeys(lastname);
ff.setJob().sendKeys(job);
ff.clickEducation().click();
ff.clickGender().click();
ff.clickYears().click();
ff.clickDate().click();
/**driver.findElement(By.cssSelector("input[id='first-name']")).sendKeys(firstname);
driver.findElement(By.cssSelector("input[id='last-name']")).sendKeys(lastname);
driver.findElement(By.cssSelector("#job-title")).sendKeys(job);

driver.findElement(By.cssSelector("#radio-button-"+education)).click();
driver.findElement(By.cssSelector("#checkbox-2")).click();
driver.findElement(By.cssSelector("#select-menu option:nth-child(2)")).click();
driver.findElement(By.cssSelector("#datepicker")).click();
driver.findElement(By.xpath("//tr/td[@class='today day']")).click();**/


ff.submit().click();

WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
wait.until(ExpectedConditions.visibilityOfElementLocated(ff.alert()));
//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
assertEquals(ff.getAlertTitle(),"The form was successfully submitted!");

}

@DataProvider(name="testdata")
public Object[][] testDataExample(){
readExcel configuration = new readExcel("C:\\Users\\ana__\\Documents\\Libro1.xlsx");
Object[][]signin_credentials = new Object[configuration.getRowCount(0)][2];

for(int i=0;i<configuration.getRowCount(0);i++)
{
signin_credentials[i][0] = configuration.getData(0, i, 0);
signin_credentials[i][1] = configuration.getData(0, i, 1);
}
return signin_credentials;
}
}