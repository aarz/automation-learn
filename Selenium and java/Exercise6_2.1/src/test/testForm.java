package test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.readExcel;

import excel.FillForm;

public class testForm{
	WebDriver driver;
@Test(dataProvider="testdata")
public void demoClass(String firstname, String lastname) throws InterruptedException, IOException {
System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver.exe");
WebDriver driver = new ChromeDriver();
driver.get("https://formy-project.herokuapp.com/form");
excel.FillForm ff = new excel.FillForm(driver);
ff.setFirstName().sendKeys(firstname);
ff.setLastName().sendKeys(lastname);
ff.setJob().sendKeys("AT");
ff.clickEducation().click();
File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
FileUtils.copyFile(scrFile, new File(".//screenshot/image1.png"));
ff.clickGender().click();
File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
FileUtils.copyFile(scrFile2, new File(".//screenshot/image2.png"));
ff.clickYears().click();
ff.clickDate().click();
File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
FileUtils.copyFile(scrFile3, new File(".//screenshot/image3.png"));
/**driver.findElement(By.cssSelector("input[id='first-name']")).sendKeys(firstname);
driver.findElement(By.cssSelector("input[id='last-name']")).sendKeys(lastname);
driver.findElement(By.cssSelector("#job-title")).sendKeys(job);

driver.findElement(By.cssSelector("#radio-button-"+education)).click();
driver.findElement(By.cssSelector("#checkbox-2")).click();
driver.findElement(By.cssSelector("#select-menu option:nth-child(2)")).click();
driver.findElement(By.cssSelector("#datepicker")).click();
driver.findElement(By.xpath("//tr/td[@class='today day']")).click();**/


ff.submit().click();

WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
wait.until(ExpectedConditions.visibilityOfElementLocated(ff.alert()));
//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
assertEquals(ff.getAlertTitle(),"The form was successfully submitted!");

}

@DataProvider(name="testdata")
public Object[][] testDataExample(){
/**readExcel configuration = new readExcel("C:\\Users\\ana__\\eclipse-workspace\\Driven\\src\\testData\\TestData.xlsx");

int rows = configuration.getRowCount(0);
Object[][]signin_credentials = new Object[rows][1];

for(int i=0;i<rows;i++)
{
signin_credentials[0][0] = configuration.getData(0, 0, 0);
signin_credentials[0][1] = configuration.getData(0, 0, 1);


}
//return signin_credentials;**/

	 return new Object[][] { { "Ana", "Ram�rez" }};
	 }
}