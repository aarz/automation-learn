package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.FillForm;



public class TestFillForm 
{

   
    @Test
    public void testFillForm() throws IOException
    {
    	
    	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/form");
        
        FillForm ff = new FillForm(driver);
    	ff.setFirstName().sendKeys("Ana");
    	ff.setLastName().sendKeys("Ramirez");
    	ff.setJob().sendKeys("AT");
    	ff.clickEducation().click();
    	File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile1, new File(".//screenshot/image1.png"));
    	ff.clickGender().click();
    	File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile2, new File(".//screenshot/image2.png"));
    	ff.clickYears().click();
    	ff.clickDate().click();
    	File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile3, new File(".//screenshot/image3.png"));
    	
    	ff.submit().click();
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
    	wait.until(ExpectedConditions.visibilityOfElementLocated(ff.alert()));
    	//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	assertEquals(ff.getAlertTitle(),"The form was successfully submitted!");

        
    }
    


}

