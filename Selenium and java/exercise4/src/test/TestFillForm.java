package test;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import pages.FillForm;



public class TestFillForm 
{
	WebDriver driver;
	FillForm ff;
	WebDriverWait wait;
	

	
	@Before

    public void setup(){

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        
        driver.get("https://formy-project.herokuapp.com/form");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
   
    @Test()
    public void testFillForm()
    {
    	ff = new FillForm(driver);
    	ff.setFirstName("Ana");
    	ff.setLastName("Ramirez");
    	ff.setJob("AT");
    	ff.clickEducation();
    	ff.clickGender();
    	ff.clickYears();
    	ff.clickDate();
    	
    	ff.methodSubmit();
    	//wait = new WebDriverWait(driver,4);
    	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
    	assertEquals(ff.getAlertTitle(),"The form was successfully submitted!");
    	
    	
        
        
    }
    
    @After()
    public void waitForm()
    {
    	
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	driver.close();
    }
}

