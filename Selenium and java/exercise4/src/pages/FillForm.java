package pages;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FillForm {
	

	    WebDriver driver;

	    By firstname = By.cssSelector("input[id='first-name']");
	    By lastname = By.cssSelector("input[id='last-name']");
	    By job = By.cssSelector("#job-title");
	    By education = By.cssSelector("#radio-button-2");
	    By gender = By.cssSelector("#checkbox-2");
	    By years = By.cssSelector("#select-menu option:nth-child(2)");
	    By date = By.cssSelector("#datepicker");
	    By todays = By.xpath("//tr/td[@class='today day']");
	    By submit = By.cssSelector("div[class='col-sm-4 col-sm-offset-2'] a");
	    By alert = By.cssSelector("div[role='alert']");

	    public FillForm(WebDriver driver){

	        this.driver = driver;

	    }

	    //Set user name in textbox

	    public void setFirstName(String strFirstName){

	        driver.findElement(firstname).sendKeys(strFirstName);

	    }

	    //Set password in password textbox

	    public void setLastName(String strLastName){

	         driver.findElement(lastname).sendKeys(strLastName);

	    }
	    
	    public void setJob(String strJob){

	         driver.findElement(job).sendKeys(strJob);

	    }
	    

	    //Click on login button

	    public void clickEducation(){

	            driver.findElement(education).click();

	    }
	    
	    public void clickGender(){

            driver.findElement(gender).click();

    }
	 
	    public void clickYears(){

            driver.findElement(years).click();

    }
	    
	    
	    public void clickDate(){

            driver.findElement(date).click();
            driver.findElement(todays).click();
            

    }
	    
	   

	    

	    public By getAlert() {
			return alert;
		}


		public String getAlertTitle(){

	     return    driver.findElement(alert).getText();

	    }

	    /**

	     * This POM method will be exposed in test case to login in the application

	     * @param strUserName

	     * @param strPasword

	     * @return

	     */

	    public void fillForm(String strFirstName,String strLastName, String strJob){

	        //Fill user name

	        this.setFirstName(strFirstName);

	        //Fill password

	        this.setLastName(strLastName);

	        //Click Login button
	        
	        this.setJob(strJob);

	        
	        this.clickEducation();
	        this.clickGender();
	        this.clickYears();
	        this.clickDate();
	        this.methodSubmit();
	    

	}

		public void methodSubmit() {
			// TODO Auto-generated method stub
			
					driver.findElement(submit).click();
					WebDriverWait wait = new WebDriverWait(driver,4);
			    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		}
	

}
