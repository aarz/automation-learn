package packageAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumExercise2 {
	
	    public static void main(String[] args) {
	        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver_win32\\chromedriver.exe");
	        WebDriver driver = new ChromeDriver();
	        driver.get("https://formy-project.herokuapp.com/form");
	        
	        driver.findElement(By.cssSelector("input[id='first-name']")).sendKeys("Ana");
	        driver.findElement(By.cssSelector("input[id='last-name']")).sendKeys("Ram�rez");
	        driver.findElement(By.cssSelector("#job-title")).sendKeys("Automation tester");

	        driver.findElement(By.cssSelector("#radio-button-2")).click();
	        driver.findElement(By.cssSelector("#checkbox-2")).click();
	        driver.findElement(By.cssSelector("#select-menu option:nth-child(2)")).click();
	        driver.findElement(By.cssSelector("#datepicker")).click();
	        driver.findElement(By.xpath("//tr/td[@class='today day']")).click();

	        //driver.findElement(By.cssSelector("div[class='col-sm-4 col-sm-offset-2'] a")).click();
	        
	       // driver.close();
	    
	}


}
