package test;

import static org.junit.Assert.*;

import java.time.Duration;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.FillForm;



public class TestFillForm 
{

   
    @Test
    public void testFillForm()
    {
    	
    	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ana__\\Downloads\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/form");
        
        FillForm ff = new FillForm(driver);
    	ff.setFirstName().sendKeys("Ana");
    	ff.setLastName().sendKeys("Ramirez");
    	ff.setJob().sendKeys("AT");
    	ff.clickEducation().click();
    	ff.clickGender().click();
    	ff.clickYears().click();
    	ff.clickDate().click();
    	
    	ff.submit().click();
    	/**WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
    	wait.until(ExpectedConditions.visibilityOfElementLocated(ff.alert()));
    	//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	assertEquals(ff.getAlertTitle(),"The form was successfully submitted!");**/
    	ff.assertAlert();

        
    }
    


}

