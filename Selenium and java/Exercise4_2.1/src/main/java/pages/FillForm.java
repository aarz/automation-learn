package pages;

import static org.junit.Assert.assertEquals;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FillForm {
	

	    WebDriver driver;

	    By firstname = By.cssSelector("input[id='first-name']");
	    By lastname = By.cssSelector("input[id='last-name']");
	    By job = By.cssSelector("#job-title");
	    By education = By.cssSelector("#radio-button-2");
	    By gender = By.cssSelector("#checkbox-2");
	    By years = By.cssSelector("#select-menu option:nth-child(2)");
	    By date = By.cssSelector("#datepicker");
	    By todays = By.xpath("//tr/td[@class='today day']");
	    By submit = By.cssSelector("div[class='col-sm-4 col-sm-offset-2'] a");
	    By alert = By.cssSelector("div[role='alert']");



	    public FillForm(WebDriver driver){

	        this.driver = driver;

	    }

	    

	    public WebElement setFirstName(){

	       return driver.findElement(firstname);

	    }

	    

	    public WebElement setLastName(){

	       return driver.findElement(lastname);

	    }
	    
	    public WebElement setJob(){

	        return driver.findElement(job);

	    }
	    


	    public WebElement clickEducation(){

	        return driver.findElement(education);

	    }
	    
	    public WebElement clickGender(){

           return driver.findElement(gender);

    }
	 
	    public WebElement clickYears(){

           return driver.findElement(years);

    }
	    
	    
	    public WebElement clickDate(){

            driver.findElement(date).click();
          return  driver.findElement(todays);
            

    }
	    
	    public WebElement submit() {
	    	return driver.findElement(submit);
	    }
	    


	    

	    public By alert() {
			return alert;
		}


		public String getAlertTitle(){

	     return    driver.findElement(alert).getText();

	    }
		
		public void assertAlert() {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
	    	wait.until(ExpectedConditions.visibilityOfElementLocated(alert));
	    	//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    	assertEquals(getAlertTitle(),"The form was successfully submitted!");
		}



	    public void fillForm(){



	        this.setFirstName();



	        this.setLastName();

	        
	        this.setJob();

	        
	        this.clickEducation();
	        this.clickGender();
	        this.clickYears();
	        this.clickDate();
	        this.submit();
	    

	}


	

}
